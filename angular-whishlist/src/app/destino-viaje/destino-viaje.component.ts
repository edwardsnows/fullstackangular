import { Component, OnInit, Input, HostBinding} from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model'; 

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css']
})
export class DestinoViajeComponent implements OnInit {
  
  nombre: string;
  @Input() destino: DestinoViaje;
  @HostBinding('attr.class') cssClasee = 'col-md-4';

  

  constructor() {
    this.nombre = "Lista de Hoteles";
  }

  ngOnInit(): void {
  }

}
